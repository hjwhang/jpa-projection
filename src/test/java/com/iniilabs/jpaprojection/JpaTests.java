package com.iniilabs.jpaprojection;

import com.iniilabs.jpaprojection.dto.projection.CommentOnly;
import com.iniilabs.jpaprojection.dto.projection.CommentSummary;
import com.iniilabs.jpaprojection.entity.Comment;
import com.iniilabs.jpaprojection.entity.Post;
import com.iniilabs.jpaprojection.repository.CommentRepository;
import com.iniilabs.jpaprojection.repository.PostRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class JpaTests {
    @Autowired
    CommentRepository commentRepo;

    @Autowired
    PostRepository postRepo;

    @Test
    public void getComment() {
        Post post = new Post();
        post.setTitle("jps");
        Post savedPost = postRepo.save(post);

        Comment comment = new Comment();
    
        comment.setComment("Hello World");
        comment.setPost(savedPost);
        comment.setUp(10);
        comment.setDown(1);
        commentRepo.save(comment);

        comment = new Comment();
        comment.setComment("Second World");
        comment.setPost(savedPost);
        comment.setUp(5);
        comment.setDown(2);
        commentRepo.save(comment);

        System.out.println("11111==============");
        commentRepo.findByPostId(savedPost.getId(), Comment.class).forEach(c -> {
            System.out.println(c);
        });
        System.out.println("222222==============");
        commentRepo.findByPostId(savedPost.getId(), CommentSummary.class).forEach( c -> {
            System.out.println(c.getComment());
        });

        System.out.println("33333==============");
        commentRepo.findByPostId(savedPost.getId(), CommentOnly.class).forEach( c -> {
            System.out.println(c.getTitle()+","+c.getPostId());
        });
    }

}
