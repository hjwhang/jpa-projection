package com.iniilabs.jpaprojection.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
public class Comment {
    @Id
    @GeneratedValue
    Long id;

    String comment;

    @ManyToOne
    Post post;

    int up;

    int down;

    boolean best;

}
