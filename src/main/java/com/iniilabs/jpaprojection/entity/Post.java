package com.iniilabs.jpaprojection.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Post {
    @Id 
    @GeneratedValue
    private Long id;

    private String title;

    private LocalDateTime created;

}
