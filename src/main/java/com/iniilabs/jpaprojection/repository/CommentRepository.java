package com.iniilabs.jpaprojection.repository;

import java.util.List;

import com.iniilabs.jpaprojection.entity.Comment;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long>{
    <T> List<T> findByPostId(Long id, Class<T> type);

}
