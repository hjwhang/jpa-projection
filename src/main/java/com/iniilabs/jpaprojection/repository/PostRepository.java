package com.iniilabs.jpaprojection.repository;

import com.iniilabs.jpaprojection.entity.Post;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long>{
    
}
