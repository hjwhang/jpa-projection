package com.iniilabs.jpaprojection.dto.projection;

public interface CommentSummary {
    String getComment();

    int getUp();

    int getDown();

    default String getVotes() {
    return "up: " + getUp() + ", down: " + getDown();
    }

}
