package com.iniilabs.jpaprojection.dto.projection;

import com.iniilabs.jpaprojection.entity.Post;

import org.springframework.beans.factory.annotation.Value;

public interface CommentOnly {
    Post getPost();
    String getComment();
    @Value("#{target.post.id}")
    Long getPostId();
    default String getTitle() {
        return getPost().getTitle();
        }
}
